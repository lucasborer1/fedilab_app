*[Home](../home) &gt;* Tips

<div align="center">
<img src="../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Tips
<br>
### View absolute time of a toot

_If you want to see the absolute time for any toot, in any timeline, or conversation just tap the relative/short date displayed on that toot. This will display the absolute time of the toot for 5 seconds, before reverting back to the relative/short time:_

* **Relative time displayed on a toot (Top-right corner)**
<br>
<img src="../res/tips/time_relative.png" width="300px">

* **Short time displayed on a toot**
<br>
<img src="../res/tips/time_short.png" width="300px">

* **Absolute time displayed on a toot**
<br>
<img src="../res/tips/time_absolute.png" width="300px">
