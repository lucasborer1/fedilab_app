#### If you like my work you can contribute with a small donation. It will help me to pay server charges and everything related to the development.

<div align="center"><b>Thank you!</b></div>
<br>

<div align="center">
	<table style="text-align: center;  width: 95%; max-width:400px;" cellspacing="0">
		<tbody>
			<tr>
				<th width="100px" style="text-align: center;">Platform</th>
				<th style="text-align: center;">Link</th>
			</tr>
			<tr>
                <td height="60px" style="text-align: center;">Liberapay<br/><img src="http://img.shields.io/liberapay/patrons/tom79.svg?logo=liberapay"></td>
            				<td>
            					<p style="text-align: center;">
            						<script src="https://liberapay.com/tom79/widgets/button.js"></script>
                                    <noscript><a href="https://liberapay.com/tom79/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a></noscript>
            					</p>
            				</td>
            			</tr>
			<tr>
				<td height="60px" style="text-align: center;">Paypal.me
				<br/><img src="../../logos/paypal-logo.png" alt="PayPal logo"/></td>
				<td>
					<p style="text-align: center;">
						<a href="https://www.paypal.me/Mastalab" target="_blank">
							<img src="../donation_buttons/paypal.gif" alt="Donate via Paypal"/>
						</a>
					</p>
				</td>
			</tr>
			<tr>
                <td height="60px" style="text-align: center;">Bitcoin
                <br/><img src="../../logos/bitcoin-logo.png" alt="Bitcoin logo"></td>
                <td>
                    <p style="text-align: center;">
                        <a href="bitcoin:3JDoJV7qf8fcmrUeYbT7hrH91PUQpg8HhS?label=fedilab" target="_blank">
                          3JDoJV7qf8fcmrUeYbT7hrH91PUQpg8HhS
                        </a>
                    </p>
                </td>
            </tr>
		</tbody>
	</table>
</div>
<br>

<h3>Thank you to 🙏</h3>

<h5>20/09</h5>
Jakub S. - Martijn DB - Shaun C. - Steffen K. - Nenghai Z.

<h5>20/08</h5>
Georges D. - Joshua M. - Jens F. 

<h5>20/07</h5>
Aslam K. -  Сергей Ж. - Dennis B. 

<h5>20/06</h5>
Poujol-Rost Mathias

<h5>20/05</h5>
Lee - Eve - Mathieu - Jose - Dave - Ondine

<h5>20/04</h5>
Karan - Matthias - Alexander - Daniel R. - Daniel K. - Dan


<h5>20/03</h5>
Thomas - Martin - Eran


<h5>20/02</h5>
Ricardo 


<h5>20/01</h5>
Detlef - John - David - Marcel - Ricardo - Luis

